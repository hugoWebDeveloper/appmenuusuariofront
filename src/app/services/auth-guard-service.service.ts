import { Injectable } from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, CanActivate, CanActivateChild, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { LoginService } from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardServiceService implements CanActivate, CanActivateChild {

  constructor(
    private router: Router,
    private loginService: LoginService
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

    const texto: any = route.data.role;

    if (localStorage.getItem('rol') == route.data.role) {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }


  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot) {



    if (localStorage.getItem('rol') == childRoute.data.role) {
      return true;
    } else {
      this.router.navigate(['notAuthorized']);
      return false;
    }
  }
}
