import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EmpleadosComponent } from './components/empleados/empleados.component';
import { HomeComponent } from './components/home/home.component';
import { JuegoComponent } from './components/juego/juego.component';
import { LoginComponent } from './components/login/login.component';
import { NotAuthorizeComponent } from './components/not-authorize/not-authorize.component';
import { AuthGuardServiceService } from './services/auth-guard-service.service';

const routes: Routes = [
  { path: "notAuthorized", component: NotAuthorizeComponent },
  {
    path: '', component: HomeComponent,
    canActivateChild: [AuthGuardServiceService],
    children: [
      { path: "juego", component: JuegoComponent, data: { role: 'jugador' }, canActivate: [AuthGuardServiceService] },
      { path: "empleados", component: EmpleadosComponent, data: { role: 'empleado' }, canActivate: [AuthGuardServiceService] }
    ]
  },
  { path: "login", component: LoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
