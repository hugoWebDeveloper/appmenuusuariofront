import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LoginService, Usuario } from 'src/app/services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string;
  contrasena: string;

  constructor(
    private loginService: LoginService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  iniciarSesion() {

    const usuario: Usuario = {
      usuario: this.usuario,
      contrasena: this.contrasena
    }

    this.loginService.iniciarSesion(usuario)
      .subscribe(result => {
        if (result != null) {

          localStorage.setItem('user',result.usuario);
          localStorage.setItem('inicioSesion', 'true');

          if (result.rolUsuario === 'empleado') {
            this.router.navigate(['/empleados']);
            localStorage.setItem('rol', 'empleado');

          }
          if (result.rolUsuario === 'jugador') {
            this.router.navigate(['/juego']);
            localStorage.setItem('rol', 'jugador');
          }

        } else {
          alert("Usuario o Contraseña incorrectas");
        }
      });
  }
}
