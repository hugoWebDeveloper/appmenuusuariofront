import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  usuario: string;
  title = 'menuUsuario';
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.usuario = localStorage.getItem("user");
  }

  salir(){
    localStorage.removeItem('user');
    localStorage.removeItem('inicioSesion');
    localStorage.removeItem('rol');
    this.router.navigate(["/login"]);
  }
}
