import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-not-authorize',
  templateUrl: './not-authorize.component.html',
  styleUrls: ['./not-authorize.component.css']
})
export class NotAuthorizeComponent implements OnInit {
  usuario: string;
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    this.usuario = localStorage.getItem("user");
  }

  salir(){
    
    localStorage.removeItem('inicioSesion');
    localStorage.removeItem('rol');
    this.router.navigate(["/login"]);
  }
}
